package com.tsystems.javaschool.tasks.subsequence;

import java.util.LinkedList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null)
            throw new IllegalArgumentException();
        if (!x.isEmpty() && y.isEmpty())
            return false;
        if (x.size() > y.size())
            return false;
        if (x.isEmpty())
            return true;

        LinkedList xLinkedList = new LinkedList(x);
        LinkedList yLinkedList = new LinkedList(y);
        Object xItem;
        Object yItem;

        while (xLinkedList.size() != 0) {
            if (yLinkedList.size() == 0)
                break;
            xItem = xLinkedList.poll();
            yItem = yLinkedList.poll();
            while (!xItem.equals(yItem) && yLinkedList.size() != 0) {
                yItem = yLinkedList.poll();
            }
        }

        if (xLinkedList.size() == 0 && yLinkedList.size() != 0)
            return true;
        return false;
    }
}
