package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.Locale;

public class Calculator {

    private static final DecimalFormat formatter = Calculator.initFormatter();

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        if (isInvalid(statement))
            return null;

        String correctedStatement = removeSpaces(statement);
        LinkedList<String> expression = makeExpression(correctedStatement);
        String result = resolve(expression);
        return result;
    }

    /**
     * This method checks whether statement:
     * - is null or empty;
     * - has two or more operation signs, any letters;
     * - has excesses of open or close parenthesis;
     * or not.
     * */
    private boolean isInvalid(String statement) {

        if (statement == null || statement.equals(""))
            return true;

        if (statement.matches("(.*)\\.{2,}(.*)") || statement.matches("(.*)\\+{2,}(.*)") || statement.matches("(.*)-{2,}(.*)")
                || statement.matches("(.*)\\*{2,}(.*)") || statement.matches("(.*)/{2,}(.*)") || statement.matches("(.*)+[a-zA-Z](.*)")
                || statement.matches("(.*)+,(.*)"))
            return true;

        int openParenthesisCounter = 0;
        int closeParenthesisCounter = 0;
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(')
                openParenthesisCounter++;
            if (statement.charAt(i) == ')')
                closeParenthesisCounter++;
        }
        if (openParenthesisCounter != closeParenthesisCounter)
            return true;

        return false;
    }

    /**
     * This method deletes any whitespaces.
     * */
    private String removeSpaces(String statement) {
        return statement.replaceAll("\\s","");
    }

    private LinkedList<String> makeExpression(String statement) {

        LinkedList<String> operators = new LinkedList<>();
        LinkedList<String> expression = new LinkedList<>();
        StringBuilder number = new StringBuilder();
        String lastOperator;

        for (int i = 0; i < statement.length(); i++) {
            String symbol = String.valueOf(statement.charAt(i));
            switch (symbol) {
                case "+": 
                    addNumToExpression(number, expression);
                    lastOperator = operators.peek();
                    if (lastOperator != null && !lastOperator.equals("(") && operators.size() != 0) {
                        expression.push(operators.pop());
                    }
                    operators.push(symbol);
                    break;
                case "-":
                    addNumToExpression(number, expression);
                    lastOperator = operators.peek();
                    if (lastOperator != null && !lastOperator.equals("(") && operators.size() != 0) {
                        expression.push(operators.pop());
                    }
                    operators.push(symbol);
                    break;
                case "*":
                    addNumToExpression(number, expression);
                    lastOperator = operators.peek();
                    if (lastOperator == null || lastOperator.equals("+") || lastOperator.equals("-") || lastOperator.equals("(")) {
                        operators.push(symbol);
                    } else {
                        if (operators.size() != 0) expression.push(operators.pop());
                        operators.push(symbol);
                    }
                    break;
                case "/": 
                    addNumToExpression(number, expression);
                    lastOperator = operators.peek();
                    if (lastOperator == null || lastOperator.equals("+") || lastOperator.equals("-") || lastOperator.equals("(")) {
                        operators.push(symbol);
                    } else {
                        if (operators.size() != 0) expression.push(operators.pop());
                        operators.push(symbol);
                    }
                    break;
                case "(":
                    addNumToExpression(number, expression);
                    operators.push(symbol);
                    break;
                case ")":
                    addNumToExpression(number, expression);
                    String operator;
                    while (!(operator = operators.pop()).equals("(")) {
                        expression.push(operator);
                    }
                    break;
                default: 
                    number.append(symbol);
                    if (i + 1 == statement.length() && !number.equals(""))
                        addNumToExpression(number, expression);
                    break;
            }
        }

        while (operators.size() != 0) {
            expression.push(operators.pop());
        }

        return expression;
    }

    private String resolve(LinkedList<String> expression) {
        LinkedList<String> result = new LinkedList<>();
        double firstOperand;
        double secondOperand;

        while (expression.size() != 0) {
            String item = expression.pollLast();
            switch (item) {
                case "+":
                    result.push(Double.toString(Double.parseDouble(result.pop()) + Double.parseDouble(result.pop())));
                    break;
                case "-":
                    secondOperand = Double.parseDouble(result.pop());
                    firstOperand = Double.parseDouble(result.pop());
                    result.push(Double.toString(firstOperand - secondOperand));
                    break;
                case "*":
                    result.push(Double.toString(Double.parseDouble(result.pop()) * Double.parseDouble(result.pop())));
                    break;
                case "/":
                    secondOperand = Double.parseDouble(result.pop());
                    if (secondOperand == 0)
                        return null;
                    firstOperand = Double.parseDouble(result.pop());
                    result.push(Double.toString(firstOperand / secondOperand));
                    break;
                default:
                    result.push(item);
                    break;
            }
        }

        return formatter.format(Double.parseDouble(result.poll()));
    }

    private void addNumToExpression(StringBuilder number, LinkedList<String> numbers) {
        if (!number.toString().equals("")) {
            numbers.push(number.toString());
            number.delete(0, number.length());
        }
    }

    private static DecimalFormat initFormatter() {
        NumberFormat numberFormat = NumberFormat.getNumberInstance(Locale.US);
        DecimalFormat decimalFormat = (DecimalFormat) numberFormat;
        decimalFormat.applyPattern("#.####");
        decimalFormat.setDecimalSeparatorAlwaysShown(false);
        return decimalFormat;
    }
}
