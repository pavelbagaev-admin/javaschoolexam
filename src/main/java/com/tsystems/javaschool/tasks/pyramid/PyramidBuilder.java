package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int rows = calculateRows(inputNumbers.size());

        if (rows == 0) throw new CannotBuildPyramidException();

        Collections.sort(inputNumbers);


        /*Building of the pyramid.*/
        int columns = 2 * rows - 1;
        int[][] pyramid = new int[rows][columns];
        int startingPosition = columns / 2;
        pyramid[0][startingPosition] = inputNumbers.get(0);
        int k = 1;
        int m;
        for (int i = 1; i < pyramid.length; i++) {
            startingPosition -= 1;
            m = startingPosition;
            for (int j = 0; j <= i; j++) {
                pyramid[i][m] = inputNumbers.get(k);
                k++;
                m += 2;
            }
        }

        return pyramid;
    }


    /**
     * Calculates amount of rows of the pyramid.
     *
     * @param arraySize size of the input array
     * @return amount of rows in the pyramid or 0 if pyramid cannot be build
     */
    private int calculateRows(int arraySize) {
        int i = 1;
        boolean isFound = false;
        double prevElement = (Math.pow(i, 2) + 3 * i + 2) / 2;
        double nextElement;

        do {
            i++;
            nextElement = (Math.pow(i, 2) + 3 * i + 2) / 2;

            /*Pyramid cannot be build if there are less than 3 elements in the initial array*/
            if (arraySize < prevElement) {
                i = 0;
                isFound = true;
            }

            /**
             * Pyramid cannot be build if the size of initial array is not equal to one of the elements of so called
             * "pyramid sequence". It's the sequence that describes how many elements are needed to build a pyramid.
             * It goes like this: 3, 6, 10, ... and calculates by following formula: x = (Math.pow(i, 2) + 3 * i + 2) / 2,
             * where i - is number of element in the sequence, starting from 1.
             */
            if (arraySize > prevElement && arraySize < nextElement) {
                i = 0;
                isFound = true;
            }

            if (arraySize == prevElement) {
                isFound = true;
            }

            if (arraySize == nextElement) {
                i += 1;
                isFound = true;
            }

            prevElement = nextElement;
        } while(!isFound);

        return i;
    }
}
